# Go Schulung Greenbone 23.08.2021 - 25.08.2021

* [Agenda](agenda.md)
* [Links](links.md)
* [Code-Beispiele](https://gitlab.com/sascha.l.teichmann/go-examples)
* [Live-Coding-Beispiele](workspace/src/README.md)


