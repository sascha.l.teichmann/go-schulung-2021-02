package main

func foo(x [192]int) {
}

func main() {

	//var x [100_000]int

	x := [...]int{
		10:  1,
		191: 3,
		40:  4,
	}

	x[0] = 1
	x[2] = 1

	for _, v := range x {
		_ = v
	}

	for i := 0; i < len(x); i++ {
		_ = i
	}

	foo(x)

}
