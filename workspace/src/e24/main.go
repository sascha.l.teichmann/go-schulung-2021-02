package main

type Foo int

func (f *Foo) setMe() {
	*f = 42
}

type MyFunc func()

func (mf MyFunc) call() {
	mf()
}

func bar(fn MyFunc) {
	fn.call()
}

func main() {

	var x Foo

	x.setMe()

	bar(x.setMe)

	/*
		bar(func() {
			x.setMe()
		})

	*/
}
