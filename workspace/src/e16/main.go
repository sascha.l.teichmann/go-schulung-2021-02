package main

import "fmt"

func main() {
	s := "Hello, 世界"

	b := []byte{1, 2, 3}

	sx := string(b)

	_ = sx

	fmt.Printf("len %d\n", len(s))

	for i, r := range s {
		fmt.Printf("%d: %c\n", i, r)
	}

	rs := []rune(s)

	for i, r := range rs {
		fmt.Printf("%d: %c\n", i, r)
	}
}
