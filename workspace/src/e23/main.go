package main

import "fmt"

func fx() int {
	return 42
}

func main() {

	var i int

	switch i := fx(); i {
	case 0:
		fmt.Println("zero")
		fallthrough
	case 1:
		fmt.Println("zero")
	case 2, 3, 4:
		fmt.Println("2 - 4")
	default:
		fmt.Println("unknown")
	}

	k := 13

	switch {
	case i < 10 && i > 5:
	case k != 13:
	}

	for cond := true; cond; cond = test() {
	}

out:
	for {
		//....
		if cond {
			break out
		}
	}

target:
	goto target
}
