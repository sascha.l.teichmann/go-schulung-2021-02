package main

import "fmt"

func foo(y, w int, z float64) {
	w = 3
}

func main() {
	x := 42
	// var x int = 42
	foo(42, x, 3.1415)
	fmt.Println(x)
}
