package main

import (
	"fmt"
)

func main() {

	//var ch chan bool

	ch := make(chan int)
	done := make(chan struct{})

	go func() {
		defer close(done)

		for i := range ch {
			fmt.Println(i)
		}

	}()

	for i := 0; i < 5; i++ {
		ch <- i
	}
	close(ch)

	<-done

	//fmt.Println(ok)
}
