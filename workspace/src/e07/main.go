package main

import (
	"fmt"
	"os"
)

func main() {

	f, err := os.Create("/tmp/passwdixx")
	if err == nil {
		defer func() {
			fmt.Println("defer called")
			f.Close()
		}()
	}

	write := func(msg string) {
		if err == nil {
			_, err = f.Write([]byte(msg))
			if err != nil {
				fmt.Printf("failed: %v\n", err)
			}
		}
	}

	write("foo\n")
	write("bar\n")

	if err != nil {
		// ....
	}

}
