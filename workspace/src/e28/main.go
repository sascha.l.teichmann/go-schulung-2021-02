package main

import "fmt"

const (
	foo = (1 + iota) * iota
	bar
	baz
	boo
)

const (
	tam = iota
)

func main() {
	fmt.Println(foo)
	fmt.Println(bar)
	fmt.Println(baz)
	fmt.Println(boo)
	fmt.Println(tam)
}
