package main

import "fmt"

func foo(x *int) {
	*x = 31
}

func bar() *int {
	var y int = 32
	return &y
}

func baz() *int {
	return new(int)
}

func main() {
	var x int
	foo(&x)
	fmt.Println(x)

	z := bar()
	*z = 42
	fmt.Println(*z)
}
