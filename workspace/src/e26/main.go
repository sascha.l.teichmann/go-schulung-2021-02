package main

import (
	"fmt"
	"runtime"
	"sync"
)

func worker(wg *sync.WaitGroup, ch <-chan int) {
	defer wg.Done()
	for x := range ch {
		fmt.Println(x)
	}
}

func main() {

	ch := make(chan int)

	n := runtime.NumCPU()
	fmt.Printf("num cpu: %d\n", n)

	var wg sync.WaitGroup

	for i := 0; i < n; i++ {
		wg.Add(1)
		go worker(&wg, ch)
	}

	for i := 0; i < 100; i++ {
		ch <- i
	}
	close(ch)

	wg.Wait()

}
