package main

import "fmt"

func print(args ...interface{}) {

	for _, v := range args {

		if x, ok := v.(int); ok {
			fmt.Printf("%T: %d\n", x, x)
		}

		switch x := v.(type) {
		case int:
			fmt.Printf("int: %d\n", x)
		case float64:
			fmt.Printf("float64: %f\n", x)
		default:
			fmt.Printf("unknown: %v\n", x)
		}
	}
}

func main() {

	print(1, 2, 3)
	print(1, "foo", 3.1415)
}
