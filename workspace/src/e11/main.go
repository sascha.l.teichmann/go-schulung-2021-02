package main

import "fmt"

func main() {

	slice := make([]int, 3, 20)

	fmt.Println(len(slice))
	fmt.Println(cap(slice))

	slice = slice[:len(slice)+1]
	fmt.Println(len(slice))
	fmt.Println(cap(slice))

	slice = append(slice, slice...)
	fmt.Println(len(slice))
	fmt.Println(cap(slice))
}
