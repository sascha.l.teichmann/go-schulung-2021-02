package main

type myType int

const (
	myTypeA myType = 1
)

func (mt myTypeA) foo() {

}

func main() {

	const pi = 3.1415

	const c = 100_000_000

	const pi3 = pi * c

	const foo float32 = 23

	const x = myTypeA
	x.foo()
}
