package main

import "fmt"

type myType float32

type herType int

type yourType struct {
	myType
	herType
}

func (ht herType) print() {
}

func (mt myType) print() {
	fmt.Println("myType print")
}

func (yt yourType) print() {
	yt.myType.print()
	yt.herType.print()
	fmt.Println("yourType print")
}

func main() {

	var x myType
	x.print()

	var y yourType
	y.herType.print()

}
