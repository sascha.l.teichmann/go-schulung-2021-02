package first

import (
	"fmt"

	_ "github.com/lib/pq"
)

func init() {
	fmt.Println("init in first")
}

func init() {
	fmt.Println("init in first v2")
}

func Foo() {
	fmt.Println("Foo")
}
