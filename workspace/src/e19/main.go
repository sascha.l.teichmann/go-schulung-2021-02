package main

import "fmt"

type Printer interface {
	print()
}

func doIt(p Printer) {
	p.print()
}

type simple int

func (s simple) print() {
	fmt.Println("simple print")
}

func main() {
	var s Printer
	var x simple
	s = x
	//s.print()
	doIt(s)
}
