package main

import "fmt"

func foo(x []int) {
	for i := range x {
		x[i] = 56
	}
}

func main() {

	var slice []int

	var arr [4]int

	slice = arr[1:]

	foo(arr[1:])

	slice[0] = 42

	slice2 := slice[1:]
	slice2[0] = 43

	fmt.Println(slice)
	fmt.Println(slice2)
	fmt.Println(arr)

	fmt.Println(cap(slice))
	fmt.Println(cap(slice))
	// 0|1|2|3
	//  [     ]
	//[     ]3

}
