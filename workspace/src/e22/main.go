package main

import "image"

func subImage(img image.Image, rect image.Rectangle) (Image, bool) {
	if si, ok := img.(interface {
		SubImage(image.Rectangle) image.Image
	}); ok {
		return si.SubImage(rect), true
	}
	return nil, false
}
