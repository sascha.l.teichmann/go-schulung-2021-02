package main

import "fmt"

func foo(bar func() int) {
	n := bar()
	fmt.Printf("num calls: %d\n", n)
}

func construct(msg string) func() int {
	var numCalls int
	return func() int {
		fmt.Println(msg)
		numCalls++
		return numCalls
	}
}

func main() {
	baz := construct("baz")
	boo := construct("boo")
	foo(baz)
	foo(boo)
	foo(boo)
	foo(boo)
	foo(boo)
}
