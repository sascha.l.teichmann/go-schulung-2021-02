package main

import "fmt"

func bar(scale int) func() (int, bool) {
	count := 0
	return func() (int, bool) {
		if count == 3 {
			return 0, false
		}
		count++
		return count * scale, true
	}
}

func forall(fn func() (int, bool)) {
	for c, ok := fn(); ok; c, ok = fn() {
		fmt.Println(c)
	}
}

func main() {

	forall(bar(1))
	forall(bar(3))

}
