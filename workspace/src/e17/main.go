package main

import "fmt"

type myType float32

func (mt myType) print() {
	fmt.Printf("Value of mt: %f\n", mt)
}

func (mt *myType) modify() {
	*mt = 10
}

func (mt *myType) doNothing() {
}

func main() {

	var mt myType = 3

	mt *= 3

	mt.print()
	mt.modify()
	mt.print()

	var p *myType // nil

	p.doNothing()
	p.modify()
}
