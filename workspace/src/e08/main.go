package main

import (
	"fmt"
	"log"
)

func boo() {
	// Error
	panic("Boo failed")
}

func baz() {
	boo()
}

func bar() {
	baz()
}

func foo() {
	bar()
}

func MyAPI() (x int, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("%v", r)
		}
	}()
	foo()
	return 0, nil
}

func main() {
	if _, err := MyAPI(); err != nil {
		log.Printf("failure: %v\n", err)
	}
}
