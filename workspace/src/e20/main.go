package main

import "fmt"

type Starter interface {
	start()
}

type Car interface {
	Starter
	stop()
}

func drive(c Car) {
	c.start()
	c.stop()
}

type Motor byte

func (m Motor) start() {
	fmt.Println("motor start")
}

type Break rune

func (b Break) stop() {
	fmt.Println("break stop")
}

func main() {

	drive(struct {
		Motor
		Break
	}{})
}
