package main

import "fmt"

/*
import "fmt"

// import "fmt"

*/

func main() {
	fmt.Println("vim-go")

	var x int
	// int int8 int16 int32 int64
	// uint uint8 uint16 uint32 uint64
	// byte: unit8
	// rune: int32
	// bool true/false
	// float32 float64
	// string ""
	// uintptr
	// complex64 complex128

	var y byte = 42

	x = int(y)

	fmt.Println(x)
}
