package main

// foo does something.
func foo(x int) (int, int) {
	return x, x * 2
}

func bar() (x int, y int) {
	x, y = 32, 42
	return
}

func main() {
	y, z := foo(42)
	_ = y

	y, z = z, y
}
