package main

import "fmt"

func foo(msg string, x ...int) {
	fmt.Printf("len: %d\n", len(x))
	for i, v := range x {
		fmt.Printf("%d: %d\n", i, v)
	}
}

func main() {
	foo("xx")
	foo("xx", 1)
	foo("xx", 1, 2)
}
