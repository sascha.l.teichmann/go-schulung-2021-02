package main

import "fmt"

func main() {

	var m map[string]int

	m = make(map[string]int)
	// m = map[string]int{}
	// m = map[string]int{
	//   "Peter": 3,
	// }

	m["Peter"] = 3
	m["Paul"] = 0
	m["Erin"] = 42
	m["Susan"] = 12

	fmt.Printf("Peter: %d\n", m["Peter"])
	fmt.Printf("Paul: %d\n", m["Paul"])
	fmt.Printf("Klaus: %d\n", m["Klaus"])

	if _, ok := m["Paul"]; ok {
		fmt.Println("Paul has a value")
	}

	if _, ok := m["Klaus"]; !ok {
		fmt.Println("Klaus has no value")
	}

	fmt.Println("-----------")
	for k, v := range m {
		fmt.Printf("%s: %v\n", k, v)
	}
}
