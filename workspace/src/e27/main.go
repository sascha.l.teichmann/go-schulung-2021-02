package main

type data struct {
	important int
	funcs     chan func(*data)
}

func (d *data) run() {
	for fn := range d.funcs {
		fn(d)
	}
}

func (d *data) add() {
	d.funcs <- func(d *data) {
		d.important++
	}
}

func main() {

	var d data
	go d.run()

	d.add()
}
