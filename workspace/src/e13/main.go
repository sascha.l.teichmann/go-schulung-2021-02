package main

func main() {
	var x = struct {
		a int
		b float64
		_ int
	}{
		a: 34,
	}

	y := struct{}{}

	x.a = 42
	x.b = 3.14

	for i := range make([]struct{}, 100_000_000) {
		_ = i
	}

	//m := make(map[string]int)
	var m map[string]int // <- nil
	m["klaus"] = 42

	s := make(map[string]bool)
}
